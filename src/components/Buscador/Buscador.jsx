export function Buscardor({ valor, onBuscar }) {
  return (
    <div className='d-flex justify-content-end'>
      <div className='mb-3 col-5'>
        <input
          type='text'
          className='form-control'
          placeholder='Buscar personaje...'
          value={valor}
          onChange={(evento) => onBuscar(evento.target.value)}
        />
      </div>
    </div>
  );
}
