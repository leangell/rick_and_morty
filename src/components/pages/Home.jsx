import { Banner } from '../Navbar/Banner';
import { Buscardor } from '../Buscador/Buscador';
import { ListadoPersonaje } from '../Personajes/ListadoPersonaje';
import { useState } from 'react';
export function Home() {
  let [buscador, setBuscador] = useState('');
  return (
    <div>
      <Banner />
      <div className='px-5'>
        <h1 className='py-4'>Personajes destacados</h1>
        <Buscardor valor={buscador} onBuscar={setBuscador} />

        <ListadoPersonaje buscar={buscador} />
      </div>
    </div>
  );
}
