import { useState, useEffect } from 'react';
import { BannerDetallePersonaje } from '../Personajes/BannerDetallePersonaje';
import { ListaEpisodio } from '../Episodio/ListaEpisodio';
import { useParams } from 'react-router-dom';
import axios from 'axios';
export function Personaje() {
  let { personajeId } = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [episodios, setEpisodios] = useState(null);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${personajeId}`)
      .then((respuesta) => {
        setPersonaje(respuesta.data);
        let { episode } = respuesta.data;

        // generamos un arreglo de promesas de cada episodio
        let peticionesEpisodios = episode.map((urlEpisodio) =>
          axios.get(urlEpisodio)
        );

        // ejecutamos todas las promesas y esperamos respuesta
        Promise.all(peticionesEpisodios).then((respuestaEpisodios) => {
          // transformamos cada episodio obteniendo la propiedad data de cada petición
          let datosFormateados = respuestaEpisodios.map(
            (episodio) => episodio.data
          );
          // guardamos los datos de nuestros episodios
          setEpisodios(datosFormateados);
        });
      });
  }, []);

  return (
    <div className='p-5'>
      {personaje ? (
        <div>
          <BannerDetallePersonaje {...personaje} />

          <h2 className='py-4'>Episodios</h2>

          {episodios ? (
            <ListaEpisodio datos={episodios} />
          ) : (
            <div>Cargando episodios...</div>
          )}
        </div>
      ) : (
        <div>Cargando...</div>
      )}
    </div>
  );
}
