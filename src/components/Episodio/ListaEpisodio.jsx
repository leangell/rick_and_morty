import { Episodio } from './Episodio';

export function ListaEpisodio({ datos }) {
  return (
    <div className='row'>
      {datos.map((episodio) => {
        let { id } = episodio;
        return <Episodio {...episodio} key={id} />;
      })}
    </div>
  );
}
