export function Episodio({ name, air_date, episode }) {
  return (
    <div className='col-sm-6 col-lg-4 col-xl-3'>
      <div
        className='card text-white  mb-3'
        style={{ maxWidth: '18rem', backgroundColor: '#8a4af1' }}
      >
        <div className='card-header'>{air_date}</div>
        <div className='card-body'>
          <h5 className='card-title'>{name}</h5>
          <p className='card-text'>{episode}</p>
        </div>
      </div>
    </div>
  );
}
