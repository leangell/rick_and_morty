export function Footer() {
  return (
    <div className='navbar-dark ' style={{ backgroundColor: '#8a4af1' }}>
      <p className='text-center py-4 mb-0 fs-4 text-white'>
        Proyecto creado por Leandro Meza CAR IV
      </p>
    </div>
  );
}
