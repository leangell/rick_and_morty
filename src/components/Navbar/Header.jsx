import { Link } from 'react-router-dom';

export function Header() {
  return (
    <nav
      className='navbar navbar-expand-lg navbar-dark '
      style={{ backgroundColor: '#8a4af1' }}
    >
      <div className='container-fluid'>
        <Link className='navbar-brand fs-3' to='/'>
          Rick and Morty
        </Link>
        <div className='collapse navbar-collapse' id='navbarText'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item'>
              <Link className='nav-link active' aria-current='page' to='/'>
                Home
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
