import bannerImg from '../../assets/imgs/banner-img3.jpg';
export function Banner() {
  return (
    <div>
      <img
        style={{ height: '1000px', objectFit: 'cover' }}
        src={bannerImg}
        className='card-img'
        alt='banner'
      />
    </div>
  );
}
