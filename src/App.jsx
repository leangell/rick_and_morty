import 'bootstrap/dist/css/bootstrap.min.css';

import { Link, Route, Routes } from 'react-router-dom';

import { Footer } from './components/Footer/Footer';
import { Header } from './components/Navbar/Header';
import { Home } from './components/pages/Home';
import { Personaje } from './components/pages/Personaje';
import { useState } from 'react';

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className='App bg-dark'>
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/personaje/:personajeId' element={<Personaje />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
